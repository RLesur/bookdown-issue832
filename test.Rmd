---
title: Cross-reference
output: bookdown::html_document2
---

# Part 1 {#p1}

## Chapter 1 {#p1ch1}

Some text.

## Chapter 2 {#p1ch2}

Here we need to refer to chapter 1 as ``` `\@ref(p1ch1)` ```: see ch. \@ref(p1ch1).
