#!/bin/bash

set -ev

declare -a versions=("2.8.0.1" "2.8" "2.7.3")

for version in "${versions[@]}";
do
    ./_build_with_pandoc_version.sh $version
done
