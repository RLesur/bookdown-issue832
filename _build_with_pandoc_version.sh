#!/bin/sh

set -ev

wget -O pandoc-$1-1-amd64.deb https://github.com/jgm/pandoc/releases/download/$1/pandoc-$1-1-amd64.deb
dpkg -i pandoc-$1-1-amd64.deb

Rscript -e "rmarkdown::render('test.Rmd', 'rmarkdown::html_document', 'rmarkdown_html_document.html', 'public/pandoc-$1', list(keep_md = TRUE))"
Rscript -e "tryCatch(rmarkdown::render('test.Rmd', 'bookdown::html_document2', 'bookdown_html_document2.html', 'public/pandoc-$1', list(keep_md = TRUE)), error = function(e) {})"
cp index.html public/pandoc-$1
cd public/pandoc-$1
Rscript -e "rmarkdown::pandoc_convert('rmarkdown_html_document.md', 'html4', output = 'pandoc_conversion_from_md.html', options = c('--standalone', '--section-divs', '--number-sections'))"
cd ../..
